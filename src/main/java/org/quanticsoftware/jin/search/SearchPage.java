package org.quanticsoftware.jin.search;

import java.util.List;
import java.util.Map;

import org.quanticsoftware.automata.core.Context;
import org.quanticsoftware.automata.core.Conversion;
import org.quanticsoftware.automata.core.DataObject;
import org.quanticsoftware.automata.facilities.Session;
import org.quanticsoftware.automata.servlet.pages.DataFormTool;
import org.quanticsoftware.automata.servlet.pages.PageDefinitionContext;
import org.quanticsoftware.automata.servlet.pages.PageDefinition;
import org.quanticsoftware.automata.servlet.pages.PageElement;
import org.quanticsoftware.jin.Shared;
import org.quanticsoftware.jin.frontend.FrontendContext;

public class SearchPage implements PageDefinition {

	@Override
	public final void execute(PageDefinitionContext pagedef) {
		pagedef.elements.get("head").title = "search";
		
		var container = Shared.execute(pagedef);
		
		container.button("home").targets.put("click", "home.page_redirect");
		
		var searchform = DataFormTool.instance(container, pagedef.pagectx.type);
		
		var config = searchform.config("username");
		config.nolabel = true;
		config.einstance = (e,n)->e.p(n);
		
		config = searchform.config("message");
		config.nolabel = true;
		config.einstance = (e,n)->e.p(n);
		
		config = searchform.config("chosen");
		config.nolabel = true;
		config.einstance = (e,n)->e.parameter(n);
		
		config = searchform.config("results");
		config.nolabel = true;
		config.einstance = (e,n)->e.div(n);
		
		searchform.config("search").nolabel = true;
		
		searchform.build();
		
		searchform.get("search_group").
			button("search").
			targets.put("click", "search_search");
		
		var history = searchform.get("search_page.results");
		history.renderer = (s,e)->renderResults(s,e);
		history.cstyle = "list-group";
		
		var translation = pagedef.pagectx.translation("pt_BR");
		translation.put("home", "Meu perfil");
		translation.put("search", "Pesquisar");
	}
	
	public static final DataObject getProfile(Context context, Session session)
			throws Exception {
		var search = session.get("search_page");
		FrontendContext sessionctx = session.sessionctx();
		var chosen = Integer.parseInt(search.getst("chosen"));
		var found = sessionctx.searchctx.found.get(chosen);
		String url = null;
		
		sessionctx.profilectx.user.url = found.url;
		
		if (found.url == null) {
			var connection = session.connection();
			
			url = Shared.urlcompose(
					connection.scheme,
					"localhost",
					connection.port,
					connection.appname);
		} else {
			url = found.url;
		}
		
		var data = sessionctx.profilectx.data = Shared.downloadUserProfile(
				url,
				sessionctx.logonctx.sessionid,
				found.id);

		var datactx = context.getContextEntry(session.sessionid()).datactx;
		var type = session.type("user_profile");
		Conversion.execute(datactx, type, data);
		
		sessionctx.profilectx.user.id = (long)data.get("id");
		sessionctx.profilectx.user.username = (String)data.get("username");
		
		return search;
	}
	
	public static final DataObject mov2ctx(Session session) {
		var output = session.get("search_list");
		FrontendContext sessionctx = session.sessionctx();
		
		@SuppressWarnings("unchecked")
		var results = (List<Map<String, Object>>)output.get("results");
		var i = 0;
		for (var item : results) {
			var found = new Found();
			found.id = (long)(int)item.get("id");
			found.type = (String)item.get("type");
			found.text = (String)item.get("name");
			sessionctx.searchctx.found.put(i++, found);
		}
		
		return output;
	}
	
	private final void renderResults(Session session, PageElement element) {
		FrontendContext sessionctx = session.sessionctx();
		
		element.clear();
		for (var key : sessionctx.searchctx.found.keySet()) {
			var skey = Integer.toString(key);
			var eitem = element.a(skey);
			eitem.cstyle = "list-group-item list-group-item-action flex-column align-items-start";
			eitem.value = sessionctx.searchctx.found.get(key).text;
			eitem.targets.put("click", "profile.page_redirect");
			eitem.cparameters.put("sourcep", "search_page.chosen");
			eitem.cparameters.put("sourcev", skey);
		}
	}
	
	public static final DataObject update(Session session) {
		var searchpage = session.output();
		
		FrontendContext sessionctx = session.sessionctx();
		@SuppressWarnings("unchecked")
		var results = (Map<String, String>)searchpage.get("results");
		
		for (var key : sessionctx.searchctx.found.keySet())
			results.put(
					Integer.toString(key),
					sessionctx.searchctx.found.get(key).text);
		
		return searchpage;
	}
}

