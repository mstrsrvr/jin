package org.quanticsoftware.jin.profileinfo;

import org.quanticsoftware.automata.servlet.pages.PageDefinition;
import org.quanticsoftware.automata.servlet.pages.PageDefinitionContext;
import org.quanticsoftware.jin.Shared;
import org.quanticsoftware.jin.frontend.FrontendContext;

public class ProfileInfoPage implements PageDefinition {

	@Override
	public void execute(PageDefinitionContext pagedef) {
		pagedef.elements.get("head").title = "profile_info";
		
		var container = Shared.execute(pagedef);
		
		container.button("home").targets.put("click", "home.page_redirect");
		
		container.ul("message").renderer = (s,e)->{
			FrontendContext sessionctx = s.sessionctx();
			
			e.clear();
			
			int i = 0;
			for (var witem : sessionctx.homectx.warnings)
				e.li(Integer.toString(i++)).value = (String)witem[0];
		};
	}

}
