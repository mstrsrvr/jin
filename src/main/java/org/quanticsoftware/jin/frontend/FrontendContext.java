package org.quanticsoftware.jin.frontend;

import org.quanticsoftware.automata.core.SessionContext;
import org.quanticsoftware.jin.logon.LogonContext;
import org.quanticsoftware.jin.profile.ProfileContext;
import org.quanticsoftware.jin.search.SearchContext;
import org.quanticsoftware.jin.settings.SettingsContext;

public class FrontendContext implements SessionContext {
	public LogonContext logonctx;
	public ProfileContext homectx, profilectx;
	public SearchContext searchctx;
	public SettingsContext settingsctx;
	
	public FrontendContext() {
		settingsctx = new SettingsContext();
		homectx = new ProfileContext();
		profilectx = new ProfileContext();
		searchctx = new SearchContext();
		logonctx = new LogonContext();
	}
}
